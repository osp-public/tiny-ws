FROM  maven:3-jdk-11 AS build

WORKDIR /usr/src/app

# First copy only pom for separate step to download dependicies
COPY pom.xml .
RUN mvn dependency:go-offline

COPY . .
RUN mvn  -Dmaven.test.skip=true package



# Build the final Image only with JRE and builded Jar
FROM openjdk:11-jre
WORKDIR /opt
COPY  --from=build /usr/src/app/target/TinyWS-0.0.2-SNAPSHOT.jar ./app.jar

EXPOSE 8080
# Java-Fix: Switch random source to urandom to avoid entropy problem
CMD java -Djava.security.egd=file:/dev/./urandom -jar app.jar
