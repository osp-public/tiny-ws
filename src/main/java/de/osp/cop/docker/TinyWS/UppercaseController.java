package de.osp.cop.docker.TinyWS;


import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UppercaseController {

    @RequestMapping(value= "/uppercase/{input}", method= RequestMethod.GET)
    public UppercaseModel uppercase(@PathVariable("input") String input) {
        UppercaseModel model = new UppercaseModel(input);
        return model;
    }
}
