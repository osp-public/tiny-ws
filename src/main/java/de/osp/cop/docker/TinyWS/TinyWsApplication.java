package de.osp.cop.docker.TinyWS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TinyWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(TinyWsApplication.class, args);
	}
}
