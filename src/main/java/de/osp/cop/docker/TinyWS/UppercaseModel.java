package de.osp.cop.docker.TinyWS;

import java.time.Instant;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class UppercaseModel {

    public final String input;
    public final String uppercase;
    public final long id;
    public final String globalId;
    public final String timestamp;

    static private final AtomicLong COUNTER = new AtomicLong();

    public UppercaseModel(String original) {
        this.input = original;
        this.uppercase = original.toUpperCase();

        this.id = COUNTER.incrementAndGet();
        this.globalId = UUID.randomUUID().toString();
        this.timestamp = Instant.now().toString();
    }
}
